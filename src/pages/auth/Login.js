import React from 'react'
import Form from '../../components/shared/Form/Form'

const Login = () => {
  return (
    <>
      <div className="row g-0">
        <div className="col-md-8 form-banner">
          <img src="https://www.usnews.com/object/image/00000186-99ac-de7d-a9bf-d9ed11a70000/gettyimages-1343539369.jpg?update-time=1677615813984&size=responsive640" alt="loginformImage" />
          {/* <h4>Login</h4> */}
        </div>
        <div className="col-md-4">
            {/* <h1 className="text-center">Login</h1> */}
          <div className="form-container">
            <Form submitButton="Login" formTitle="Login" formType="Login" />
            
          </div>
        </div>
      </div>
    </>
  )
}

export default Login