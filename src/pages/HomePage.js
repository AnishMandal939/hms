import React from 'react'
import Banner from '../components/Banner'
import Navbar from '../components/Navbar'
import KeyFeatures from '../components/HPC/KeyFeatures'
import TotalClients from '../components/HPC/TotalClients'
import Footer from '../components/HPC/Footer'
// import { Link } from 'react-router-dom'

const HomePage = () => {
  return (
    <div>
      {/* <Link to="/login">Login</Link> */}
      {/* <Link to="/register">Register</Link> */}
      <Navbar />
      <Banner />
      <div className="banner-main mt-2 py-4">
        <h3 className='key__features__title'>Key Features</h3>
        <div className="card__wrapper">
          <KeyFeatures i_icon={'fa fa-clipboard'} feature_title={'APPOINTMENT MANAGEMENT'} feature_desc={'Allow patients to schedule and fix appointments with the available doctors with a single click.'} />
          <KeyFeatures i_icon={'fa fa-user-doctor'} feature_title={'DOCTOR RECORDS'} feature_desc={'Make a complete profile of doctors and manage their availability and schedule their meetings.'} />
          <KeyFeatures i_icon={'fa fa-user-doctor'} feature_title={'IN-PATIENT MANAGEMENT'} feature_desc={'Manage records of in-patients along with their admission details, bed number, diet, payments and case scenario.'} />
          <KeyFeatures i_icon={'fa fa-user-doctor'} feature_title={'OUTPATIENT MANAGEMENT'} feature_desc={'Manage payments, services received, medicines and lab reports of patients who are not admitted to the hospital.'} />
          <KeyFeatures i_icon={'fa fa-money-bill'} feature_title={'BILLINGS'} feature_desc={'Manage all transactions related to payments and invoices.'} />
          <KeyFeatures i_icon={'fa fa-bed'} feature_title={'BED MANAGEMENT SYSTEM'} feature_desc={'Allocate and ensure a unique bed number for each patient.'} />
          <KeyFeatures i_icon={'fa fa-flask'} feature_title={'LABORATORY MANAGEMENT'} feature_desc={'Keep detailed records of the tests performed on each patient.'} />
          <KeyFeatures i_icon={'fa fa-prescription fa-doctor'} feature_title={'PHARMACY MANAGEMENT'} feature_desc={'Keep track of medicines prescribed and given to each patient.'} />
        </div>
      </div>

      <div className="container mt-2 py-3">
        <h3 className='text-center fw-bold m-auto w-50'>Trusted by over 1,500 businesses around the world</h3>
        <div className="d-flex gap-2">
        <TotalClients clientSrc='jashn.jpg' />
        <TotalClients clientSrc='jashn.jpg' />

        <TotalClients clientSrc='jashn.jpg' />
        <TotalClients clientSrc='jashn.jpg' />
        <TotalClients clientSrc='jashn.jpg' />
        <TotalClients clientSrc='jashn.jpg' />
        <TotalClients clientSrc='jashn.jpg' />

        <TotalClients clientSrc='jashn.jpg' />
        <TotalClients clientSrc='jashn.jpg' />
        <TotalClients clientSrc='jashn.jpg' />

        </div>

      </div>
    <Footer />

    </div>
  )
}

export default HomePage