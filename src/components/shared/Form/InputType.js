import React from 'react'

const InputType = ({inputId,inputLabel, inputType,placeholder,input_value,inputName, inputCh  }) => {
  return (
    <>
        <div className="form-group mb-3">
            <label htmlFor={inputId}>{inputLabel}</label>
            <input type={inputType} id={inputId} placeholder={placeholder} value={input_value} name={inputName} onChange={inputCh} className={`form-control shadow-sm {inputClass}`} />
        </div>
    </>
  )
}

export default InputType