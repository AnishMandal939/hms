import React, { useState } from 'react'
import InputType from './InputType'
import { Link } from 'react-router-dom';
import Navbar from '../../Navbar';
import { handleLogin, handleRegister } from '../../../services/authService';

const Form = ({formType,submitButton,formTitle}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [role, setRole] = useState('donar');
  const [name, setName] = useState('');
  const [organizationName, setOrganizationName] = useState('');
  const [hospitalName, setHospitalName] = useState('');
  const [website, setWebsite] = useState('');
  const [address, setAddress] = useState('');
  const [phone, setPhone] = useState('');

  return (
    <>
    <Navbar />
      <form onSubmit={(e)=>{
        if(formType === 'Login') return handleLogin(e,email,password, role);
        else if(formType === 'Register') return handleRegister(e,name,email,password,role,organizationName,hospitalName,website,address,phone);
      }}>
        <h1 className='text-center'>{formTitle}</h1>
        <hr />
        <div className="d-flex mb-3">
          <div className="form-check">
            <input
              type="radio"
              className="form-check-input"
              name="role"
              id="donarRadio"
              value={"donar"}
              onChange={(e) => setRole(e.target.value)}
              defaultChecked
            />
            <label htmlFor="donarRadio" className="form-check-label">
              Donar
            </label>
          </div>
          <div className="form-check ms-2">
            <input
              type="radio"
              className="form-check-input"
              name="role"
              id="adminRadio"
              value={"admin"}
              onChange={(e) => setRole(e.target.value)}
            />
            <label htmlFor="adminRadio" className="form-check-label">
              Admin
            </label>
          </div>
          <div className="form-check ms-2">
            <input
              type="radio"
              className="form-check-input"
              name="role"
              id="hospitalRadio"
              value={"hospital"}
              onChange={(e) => setRole(e.target.value)}
            />
            <label htmlFor="hospitalRadio" className="form-check-label">
              Hospital
            </label>
          </div>
          <div className="form-check ms-2">
            <input
              type="radio"
              className="form-check-input"
              name="role"
              id="organizationRadio"
              value={"organization"}
              onChange={(e) => setRole(e.target.value)}
            />
            <label htmlFor="organizationRadio" className="form-check-label">
              Organization
            </label>
          </div>
        </div>
        {/* switch statement */}
        {/* calling IIFE */}
        {(()=>{
          // eslint-disable-next-line 
          switch(true){
            case formType === 'Login':{
              return (
                <>
                <InputType inputId="email" inputLabel="Email" inputType="email" placeholder="Email" inputName={'email'} input_value={email} inputCh={(e)=> setEmail(e.target.value)}  />
                <InputType inputId="password" inputLabel="Password" inputType="password" placeholder="Password" inputName={'password'} input_value={password} inputCh={(e)=> setPassword(e.target.value)} />
                </>
              );
            }
          case formType === 'Register':{
            return (
              <>
              {(role === 'admin' || role === 'donar') && (
                  <InputType inputId="name" inputLabel="Name" inputType="name" placeholder="Name" inputName={'name'} input_value={name} inputCh={(e)=> setName(e.target.value)} />
              )}
              {role === 'organization' && (
                <InputType inputId="organizationName" inputLabel="Organization Name" inputType="organizationName" placeholder="Organization Name" inputName={'organizationName'} input_value={organizationName} inputCh={(e)=> setOrganizationName(e.target.value)} />
              )}
              {role === 'hospital' && (
                <InputType inputId="hospitalName" inputLabel="Hospital Name" inputType="hospitalName" placeholder="Hospital Name" inputName={'hospitalName'} input_value={hospitalName} inputCh={(e)=> setHospitalName(e.target.value)} />

              )}
              <InputType inputId="email" inputLabel="Email" inputType="email" placeholder="Email" inputName={'email'} input_value={email} inputCh={(e)=> setEmail(e.target.value)}  />
              <InputType inputId="password" inputLabel="Password" inputType="password" placeholder="Password" inputName={'password'} input_value={password} inputCh={(e)=> setPassword(e.target.value)} />
              {/* <InputType inputId="role" inputLabel="Role" inputType="role" placeholder="Role" inputName={'role'} input_value={role} inputCh={(e)=> setRole(e.target.value)} /> */}

              
              <InputType inputId="website" inputLabel="Website" inputType="website" placeholder="Website" inputName={'website'} input_value={website} inputCh={(e)=> setWebsite(e.target.value)} />
              <InputType inputId="address" inputLabel="Address" inputType="address" placeholder="Address" inputName={'address'} input_value={address} inputCh={(e)=> setAddress(e.target.value)} />
              <InputType inputId="phone" inputLabel="Phone" inputType="text" placeholder="Phone" inputName={'phone'} input_value={phone} inputCh={(e)=> setPhone(e.target.value)} />
              </>
            )}
          }
        })()}
        
        
        <div className="d-flex">
          {
            formType === 'Login' ? (
              <div className='text-center'>
                <span>Don't Have an Account ? </span>
                <Link to='/register' className=" text-decoration-none shadow-sm mx-3 p-2">Register</Link>
              </div>
            ) : (
              <div className='text-center'>
                <span>Already Have an Account ? </span>
                <Link to='/login' className=" text-decoration-none shadow-sm mx-3 p-2">Login</Link>
              </div>
            )
          }

        <button type='submit' className="btn btn-sm btn-primary btn-block shadow-sm">{submitButton}</button>
        </div>

      </form>
    </>
  )
}

export default Form