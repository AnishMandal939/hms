import React from 'react'

const KeyFeatures = ({feature_title, feature_desc, i_icon}) => {
  return (
    <>
        <div className="col-3">
            <div className="card border-0 bg-white">
                <div className="card-body d-flex flex-column align-start key_card  features_height">
                <i className={`${i_icon} text-info fs-3 icon_class`} />
                    <h5 className="card-title feature__titleName">{feature_title}</h5>
                    <p className="card-text feature__description">{feature_desc}</p>
                </div>
            </div>
        </div>
    </>
  )
}

export default KeyFeatures