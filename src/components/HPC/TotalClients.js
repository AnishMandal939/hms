import React from 'react'

const TotalClients = ({clientSrc}) => {
  return (
    <>
        <div className="mt-2 py-3 border border-light p-3">
            <img src={`/images/${clientSrc}`} alt="client" className="img-fluid" />
        </div>
    </>
  )
}

export default TotalClients