import React from 'react'
import { Link } from 'react-router-dom'
import InputType from '../../components/shared/Form/InputType'

const Footer = () => {
  return (
    <>
        <footer className="footer">
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6 col-12">
                        <div className="footer__about">
                            <h3 className="footer__title text-start">About Us</h3>
                            <p className="footer__text text-start">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, voluptatum.</p>
                            <div className="footer__social flex-start">
                                <Link to="https://www.facebook.com/" target="_blank" className="footer__social-link"><i className="fa-brands fa-facebook"></i></Link>
                                <Link to="https://www.twitter.com/" target="_blank" className="footer__social-link"><i className="fa-brands fa-twitter"></i></Link>
                                <Link to="https://www.instagram.com/" target="_blank" className="footer__social-link"><i className="fa-brands fa-instagram"></i></Link>
                                <Link to="https://www.linkedin.com/" target="_blank" className="footer__social-link"><i className="fa-brands fa-linkedin"></i></Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-12">
                        <div className="footer__contact">
                            <h3 className="footer__title text-start">Contact Us</h3>
                            <ul className='list-unstyled text-start'>
                                <li> <i className="fa fa-map-marker"></i> 123 Street, Biratnagar, Nepal</li>
                                <li> <i className="fa fa-phone"></i> +(77 9862170694)</li>
                                <li> <i className="fa fa-envelope"></i>
                                    <Link to="mailto:anishmandal694@gmail.com"></Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-12">
                        <div className="footer__newsletter">
                            <h3 className="footer__title text-start">Newsletter</h3>
                            <form action="#" className="footer__newsletter-form">
                                <InputType type="email" placeholder="Enter email address" />
                                <button className="footer__newsletter-btn btn btn-sm btn-success text-start outline-none shadow-sm" type="submit">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </footer>
    </>
  )
}

export default Footer