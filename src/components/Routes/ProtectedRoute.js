import React, {useEffect} from 'react'
import {useDispatch} from 'react-redux';
import API from '../../services/API';
import { getCurrentUser } from '../../redux/features/auth/authActions';
import { Navigate } from 'react-router-dom';

const ProtectedRoute = ({children}) => {
    const dispatch = useDispatch();

    // get user current
    const getUser = async() =>{
        try {
            const {data} = await API.get('/auth/current-user')
            if(data?.success){
                dispatch(getCurrentUser(data));
            }
            
        } catch (error) {
            localStorage.clear();
            if(error.response && error.response.data.message){
                return error.response.data.message;
            }else{
                return error.message;
            }
        }
    }
    useEffect(() => {
        getUser();
        // eslint-disable-next-line
    }, [])

    if(localStorage.getItem('token')){
       return children;
    }else{
        return <Navigate to="/login" />;
    }
}

export default ProtectedRoute