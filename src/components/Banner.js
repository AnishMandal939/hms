import React from 'react'
import '../App.css';

const Banner = () => {
  return (
    <>
    <div className="banner-main">
        <div className="container py-4 d-flex g-0">
            <div className="col-6">
                <h1 className="banner-heading">Streamline and automate your hospital operations using  Hospital Management Software</h1>
                <button className='btn btn-success text-white fw-bold rounded-sm bold'>Get In Touch</button>
            </div>
            <div className="col-6 banner-image">
                <img className='img-fluid' src="https://www.usnews.com/object/image/00000186-99ac-de7d-a9bf-d9ed11a70000/gettyimages-1343539369.jpg?update-time=1677615813984&size=responsive640" alt="loginformImage" />
            </div>
        </div> 
    </div>
    </>
  )
}

export default Banner