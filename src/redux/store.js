import {configureStore} from '@reduxjs/toolkit';
import authSlice from './features/auth/authSlice';

const store = configureStore({
    reducer:{
        // reducers
        auth: authSlice.reducer,
    },
});

export default store;