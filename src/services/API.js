import axios from 'axios';

const API = axios.create({
    baseURL: process.env.REACT_APP_BASEURL
});
// interceptor to add the token to the request
API.interceptors.request.use((req) => {
    if(localStorage.getItem("token")){
        req.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
    }
    return req;
}
);

export default API;
// based on interceptor we can do CRUD operations

