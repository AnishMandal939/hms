import { userLogin, userRegister } from '../redux/features/auth/authActions';
import store from '../redux/store';

export const handleLogin = (e, email, password, role) =>{
    e.preventDefault();
    try {
        if(!email || !password || !role) {return alert('All fields are required');}
        // console.log( 'login',e, email, password, role);
        store.dispatch(userLogin({email, password, role}));
    } catch (error) {
        console.log(error);
    }
};

export const handleRegister = (e,name,email,password,role,organizationName,hospitalName,website,address,phone) =>{
    e.preventDefault();
    try {
        // console.log( 'register',e,name,email,password,organizationName,hospitalName,website,address,phone,role);
        store.dispatch(userRegister({name,email,password,organizationName,hospitalName,website,address,phone,role}));

    } catch (error) {
        console.log(error);
    }
};